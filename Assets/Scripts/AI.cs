﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/*****************************************************************************************************************************
* Write your AI code in this file here. The private variable 'agentScript' contains all the agents actions which are listed
* below. Ensure your code it clear and organised and commented.
* 
* 'agentScript' properties
* -  public bool Alive                                  // Check if we are still alive
* -  public bool PowerUp                                // Check if we have used the power up
* -  public int CurrentHitPoints                        // How many current hit points do we have
*
* 'agentScript' methods    
* -  public void MoveTo(GameObject target)               // Move towards a target object        
* -  public void RandomWander()                          // Randomly wander around the level    
* -  public bool IsInAttackRange(GameObject enemy)       // Check if we're with attacking range of the enemy    
* -  public void AttackEnemy(GameObject enemy)           // Attack the enemy
* -  public void Flee(GameObject enemy)                  // Run away
* -  public bool IsObjectInView(String name)             // Check if something of interest is in range
* -  public GameObject GetObjectInView(String name)      // Get a percieved object, null if object is not in view
* 
*****************************************************************************************************************************/

public class AI : MonoBehaviour
{
    // This is the script containing the AI agents actions
    // e.g. agentScript.MoveTo(enemy);
    private AgentActions agentScript;

    public FiniteStateMachine FSM;
   // GameObject CurrentGoTo;
    // Use this for initialization
    void Start()
    {
        agentScript = this.gameObject.GetComponent<AgentActions>();//Grab

        BuildStateMachine();//Build the state machine
    }

    // Update is called once per frame
    void Update()
    {
        FSM.OnUpdate();// use this update to execute your AI algorithm

        Debug.Log("Current Health is: " + agentScript.CurrentHitPoints);

    }

    void BuildStateMachine()
    {
        FSM = new FiniteStateMachine();
        FSM.GenerateFSM();

        State Default = new State();
        Default.Delegates = agentScript.RandomWander;
        Default.StateName = "Default";

        State PowerUpHunt = new State();
        PowerUpHunt.Delegates = MoveToPowerUp;
        PowerUpHunt.StateName = "Power Up Hunt";

        State EnemyAttack = new State();
        EnemyAttack.Delegates = AttackEnemy;
        EnemyAttack.StateName = "Enemy Attack!!";

        State EnemyFlee = new State();
        EnemyFlee.Delegates = FleeEnemy;
        EnemyFlee.StateName = "Run Away!";

        State HealthPackHunt = new State();
        HealthPackHunt.Delegates = MoveToHealthkit;
        HealthPackHunt.StateName = "Health Pack Found and Needed";

        State ResumeRandomWander = new State();
        ResumeRandomWander.Delegates = ForceReturnToDefault;
        ResumeRandomWander.StateName = "ResumeRandomWander";

        FSM.AddState(Default, null);
        FSM.AddState(PowerUpHunt, CheckForPowerup);
        FSM.AddState(EnemyAttack, EnemyInRangeAndMorePowerful);
        FSM.AddState(EnemyFlee, EnemyInRangeAndWeaker);
        FSM.AddState(HealthPackHunt, CheckForHealthKit);
        FSM.AddState(ResumeRandomWander, IsEnemyFleeing);

        

        
        FSM.CurrentState = Default;


    }//Builds each state in the state machine and generates it. 

    void MoveToPowerUp()
    {
        agentScript.MoveTo(agentScript.GetObjectInView("Power Up"));
    }//Moves the player to the powerup

    void MoveToHealthkit()
    {
        agentScript.MoveTo(agentScript.GetObjectInView("Health Kit"));
    }//Moves the player to the health kit

    bool EnemyInRangeAndMorePowerful()
    {
        bool b = false;
        AgentActions ES = (AgentActions)agentScript.Enemy.GetComponent("AgentActions");
        if (agentScript.IsInAttackRange(agentScript.Enemy) && agentScript.GetCurrentAttackPower() >= ES.GetCurrentAttackPower())
        {
            Debug.Log("Wild Enemy appeared!");
            b = true;
        }
        else
        {
            b = false;
        }

        return b;
    }//returns true if the enemy is in range and more powerful, otherwise it's false

    bool EnemyInRangeAndWeaker()
    {
     //   Debug.Log("Enemy is current in rage: " + agentScript.IsInAttackRange(agentScript.Enemy));
        bool b = false;
        AgentActions ES = (AgentActions)agentScript.Enemy.GetComponent("AgentActions");
        if (agentScript.IsInAttackRange(agentScript.Enemy) && agentScript.GetCurrentAttackPower() < ES.GetCurrentAttackPower())
        {
            b = true;
        }
        else
        {
            b = false;
        }

        return b;
    }//returns true if the enemy is in range and weaker, otherwise false.

    void AttackEnemy()
    {
        agentScript.MoveTo(agentScript.Enemy);
        agentScript.AttackEnemy(agentScript.Enemy);
    }//Attack the enemy 

    void FleeEnemy()
    {
        agentScript.Flee(agentScript.Enemy);
    }//Run away from the enemy

    bool CheckForPowerup()
    {

        bool B = false;
        if(agentScript.IsObjectInView("Power Up") && !agentScript.HasPowerUp)
        {
            B = true;
        }

        return B;
    }//Can I see a powerup

    bool CheckForHealthKit()
    { 
        bool B = false;
        if(agentScript.IsObjectInView("Health Kit") && HealthBellow25Percent())
        {
            B = true;
        }
        return B;
    }//Can I see a health kit

    bool HealthBellow25Percent()
    {
        int HealthM = 100 / agentScript.MaxHitPoints;
        int Health = HealthM * agentScript.CurrentHitPoints;
        if(Health < 25)
        {
            return true;
        }
        else
        {
            return false;
        }
    }//Is my health bellow 25%

    bool IsEnemyFleeing()
    {
        AI A = (AI)agentScript.Enemy.GetComponent("AI");
        if(A.FSM.CurrentState.StateName == "Run Away!" && FSM.CurrentState.StateName != "Default")
        {
            return true;
        }
        else
        {
            return false;
        }
    }//Is the enemy fleeing

    void ForceReturnToDefault()
    {
        FSM.ChangeState(FSM.GetState("Default"));
    }//Return to default state
}


public class FiniteStateMachine
{
    public delegate bool Watching();

    public State CurrentState;//The current active state

    public State[] StateList;//The list of states

    public Watching[] StateChanges;//The list of changes that activvate the numerically same state in state list

    int DefaultState = 0;//Default state - always 0 who doesn't code default first. 

    public void GenerateFSM()
    {
        StateList = new State[0];
        StateChanges = new Watching[0];
    }//Builds the FSM arrays to prevent errors. 

    public void AddState(State StateToAdd, Watching StateConditionChange)
    {
        State[] Old = StateList;
        Watching[] OldW = StateChanges;

        StateList = new State[Old.Length + 1];
        StateChanges = new Watching[OldW.Length + 1];

        int i = 0;
        while(i != Old.Length)
        {
            StateList[i] = Old[i];
            i++;
        }
        i = 0;
        while (i != OldW.Length)
        {
            StateChanges[i] = OldW[i];
            i++;
        }

        StateList[StateList.Length - 1] = StateToAdd;
        StateChanges[StateChanges.Length - 1] = StateConditionChange;

    }//Resizes the state list array and adds the state to it. 

    public void OnUpdate()
    {
        CurrentState.OnStateUpdate();//Run the state update.

        if (DefaultStateCheck() && CurrentState != StateList[DefaultState])//Is there any other state other than default that needs to run
        {
            ChangeState(DefaultState);//Okay cool, then run default

        }
        else//Oh, there's another state ready to go. 
        {
            int i = 1 + DefaultState;//start from next state.
            while(i != StateChanges.Length)//Find the state
            {
                if(StateChanges[i]() == true) { ChangeState(i); }//If this state is the one we're changing to. Change to it.
                i++;
            }//Go through.
        }

        Debug.Log("Current state is: " + CurrentState.StateName);
    }//State machine update.

    public void ChangeState(int NewState)
    {
        CurrentState.OnStateEnd();//Call state end.
        CurrentState = StateList[NewState];//crun current state
        CurrentState.OnStateStart();//Call state start
    }//Changes the current state.

    public int GetState(string StateName)
    {
        int i = 0;
        while(i != StateList.Length)
        {
            if(StateList[i].StateName == StateName)
            {
                break;
            }
            i++;
        }

        return i;
    }//Get the states int by name.

    bool DefaultStateCheck()
    {
        bool b = false;
        bool a = false;
        int i = 1 + DefaultState;
        while(i != StateChanges.Length)
        {
            if(StateChanges[i]() == true) { b = true; break; }
            i++;
        }

        a = !b;

        return a;
    }//Are we reading for default state
}

public class State
{
    public string StateName;//State name
    public delegate void MyDelegate();
    public MyDelegate Delegates;//Delegate ran

    public void OnStateStart()
    {
    }

    public void OnStateUpdate()
    {
        Delegates(); //Run your delegate.

      //  if(VariablesToWatch() == true)
        {
        //    Debug.Log("Success");
        }
    }
   public void OnStateEnd()
    {

    }

    
}


